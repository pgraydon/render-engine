use rand::distributions::Uniform;
use rand::prelude::Distribution;
use rand::thread_rng;
use std::f64::consts::PI;

use crate::geometry::*;
use crate::ray::Ray;
use crate::vector::Vector;

fn random_cos(normal: &Vector, uniform: Uniform<f64>) -> Vector {
    let mut rng = thread_rng();

    let r1: f64 = uniform.sample(&mut rng);
    let r2: f64 = uniform.sample(&mut rng);
    let r = (1.0 - r2).sqrt();

    let x = (2.0 * PI * r1).cos() * r;
    let y = (2.0 * PI * r1).sin() * r;
    let z = r2.sqrt();

    let mut tangent1 = if (normal.0.abs() <= normal.1.abs()) && (normal.0.abs() <= normal.2.abs()) {
        Vector::new(0.0, -normal.2, normal.1)
    } else if (normal.1.abs() <= normal.2.abs()) && (normal.1.abs() <= normal.0.abs()) {
        Vector::new(-normal.2, 0.0, normal.0)
    } else {
        Vector::new(-normal.1, normal.0, 0.0)
    };

    tangent1.normalize();
    let tangent2 = normal.cross(&tangent1);
    *normal * z + tangent1 * x + tangent2 * y
}

pub struct Scene {
    pub camera: Vector,
    pub fov: f64,
    pub focus_dist: f64,
    pub objects: Vec<Geometry>,
    pub intensity: f64,
    pub n: f64,
    uniform: Uniform<f64>,
}

impl Scene {
    pub fn new(camera: Vector, fov: f64, focus_dist: f64, intensity: f64, n: f64) -> Scene {
        Scene {
            camera,
            fov,
            focus_dist,
            objects: Vec::new(),
            intensity,
            n,
            uniform: Uniform::new(0.0, 1.0),
        }
    }

    pub fn add_object(&mut self, geo: Geometry) {
        self.objects.push(geo);
    }

    pub fn get_light_sources(&self) -> Vec<&Geometry> {
        self.objects
            .iter()
            .filter(|obj| match obj {
                Geometry::SPHERE(s) => s.surface == Surface::LIGHTSOURCE,
                Geometry::MESH(m) => m.surface == Surface::LIGHTSOURCE,
            })
            .collect()
    }

    pub fn intersect(&self, r: &Ray) -> Intersection {
        let mut intersects = false;
        let mut t_min: f64 = f64::MAX;
        let mut object_id: usize = 0;
        let mut inter_point = Vector(0.0, 0.0, 0.0);
        let mut inter_normal = Vector(0.0, 0.0, 0.0);

        for i in 0..self.objects.len() {
            let intersection = self.objects[i].intersect(r);

            if intersection.intersects {
                intersects = true;

                let local_t = intersection.t;
                let local_inter_point = intersection.inter_point;
                let local_inter_normal = intersection.inter_normal;

                if local_t < t_min {
                    t_min = local_t;
                    object_id = i;
                    inter_point = local_inter_point;
                    inter_normal = local_inter_normal;
                }
            }
        }

        Intersection {
            intersects,
            t: t_min,
            object_id,
            inter_point,
            inter_normal,
        }
    }

    pub fn get_color(&self, r: &Ray, _depth: i32, _lambertian_last_bounce: bool) -> Vector {
        if _depth <= 0 {
            return Vector::new(0.0, 0.0, 0.0);
        }

        let light_sources = self.get_light_sources();

        let intersection = self.intersect(r);
        let object_id = intersection.object_id;
        let inter_point = intersection.inter_point;
        let inter_normal = intersection.inter_normal;

        if intersection.intersects {
            match self.objects[object_id].get_surface() {
                Surface::LIGHTSOURCE => match &self.objects[object_id] {
                    Geometry::SPHERE(s) => {
                        if _lambertian_last_bounce {
                            return Vector::new(0.0, 0.0, 0.0);
                        } else {
                            return Vector::new(1.0, 1.0, 1.0)
                                * (self.intensity / (4.0 * (PI * s.r).powi(2)));
                        }
                    }
                    Geometry::MESH(_) => unimplemented!(),
                },
                Surface::DIFFUSE => {
                    let albedo = &self.objects[object_id].get_albedo();

                    let mut color = Vector::new(0.0, 0.0, 0.0);

                    /*
                    let mut vec_lum = self.light_source - inter_point;
                    let vec_lum_sq_norm = vec_lum.sq_norm();
                    vec_lum.normalize();

                    let mut color = Vector::new(0.0, 0.0, 0.0);

                    let intersection_lum =
                        &self.intersect(&Ray::new(inter_point + inter_normal * 0.001, vec_lum));
                    let intersects_lum = intersection_lum.intersects;
                    let t_shadow = intersection_lum.t;
                    let shadow_exists = intersects_lum && (t_shadow * t_shadow < vec_lum_sq_norm);

                    if !shadow_exists {
                        color = *albedo
                            * inter_normal.dot(&vec_lum).max(0.0)
                            * (self.intensity / (4.0 * PI * PI * vec_lum_sq_norm));
                    }
                    */

                    for src in light_sources {
                        match src {
                            Geometry::SPHERE(s) => {
                                let mut vec_lum = s.c - inter_point;
                                vec_lum.normalize();

                                let vec_x_prime = random_cos(&-vec_lum, self.uniform);
                                let x_prime = vec_x_prime * s.r + s.c;

                                let p_x_prime =
                                    -vec_lum.dot(&vec_x_prime) * (1.0 / (PI * s.r.powi(2)));

                                let mut vec_xx_prime = x_prime - inter_point;
                                let vec_xx_prime_norm2 = vec_xx_prime.sq_norm();
                                vec_xx_prime.normalize();

                                // Compute intersection for shading
                                let intersection_lum = &self.intersect(&Ray::new(
                                    inter_point + inter_normal * 0.001,
                                    vec_xx_prime,
                                ));
                                let intersects_lum = intersection_lum.intersects;
                                let t_shadow = intersection_lum.t;
                                let shadow_exists = intersects_lum
                                    && (t_shadow * t_shadow < vec_xx_prime_norm2 * 0.95);
                                let weighted_intensity =
                                    self.intensity / (4.0 * (PI * s.r).powi(2));

                                // Compute lambertian colors only if there is no shadow at that point
                                if !shadow_exists {
                                    color += *albedo
                                        * inter_normal.dot(&vec_xx_prime).max(0.0)
                                        * vec_x_prime.dot(&-vec_xx_prime).max(0.0)
                                        * (weighted_intensity
                                            / (vec_xx_prime_norm2 * p_x_prime * PI));
                                }
                            }
                            Geometry::MESH(_) => unimplemented!(),
                        };
                    }

                    // Compute indirect lighting component
                    let omega_i = random_cos(&inter_normal, self.uniform);
                    let indirect_color = *albedo
                        * self.get_color(
                            &Ray::new(inter_point + inter_normal * 0.001, omega_i),
                            _depth - 1,
                            true,
                        );

                    color += indirect_color;

                    return color;
                }
                Surface::MIRROR => {
                    let reflection = r.u - inter_normal * 2.0 * r.u.dot(&inter_normal);
                    let reflected_ray = Ray::new(inter_point + inter_normal * 0.0001, reflection);

                    return self.get_color(&reflected_ray, _depth - 1, false);
                }
                Surface::TRANSPARENT => {
                    let n_object = self.objects[object_id].get_refraction_index();

                    // Check incoming vector direction
                    let inside_object = r.u.dot(&inter_normal) > 0.0;

                    let corrected_normal = if inside_object {
                        -inter_normal
                    } else {
                        inter_normal
                    };
                    let n_in = if inside_object { n_object } else { self.n };
                    let n_out = if inside_object { self.n } else { n_object };

                    let cos_theta_in = r.u.dot(&corrected_normal);

                    if (1.0 - cos_theta_in * cos_theta_in).sqrt() < (n_out / n_in) {
                        // Compute refracted ray components
                        let refraction_tangent =
                            (r.u - corrected_normal * cos_theta_in) * (n_in / n_out);
                        let refraction_normal = corrected_normal
                            * -(1.0
                                - (n_in / n_out)
                                    * (n_in / n_out)
                                    * (1.0 - cos_theta_in * cos_theta_in))
                                .sqrt();
                        let refraction = refraction_tangent + refraction_normal;

                        let refracted_ray =
                            Ray::new(inter_point - corrected_normal * 0.001, refraction);
                        return self.get_color(&refracted_ray, _depth - 1, false);
                    } else {
                        // Compute totally reflected ray
                        let reflection = r.u - corrected_normal * 2.0 * r.u.dot(&corrected_normal);
                        let reflected_ray =
                            Ray::new(inter_point + corrected_normal * 0.0001, reflection);

                        return self.get_color(&reflected_ray, _depth - 1, false);
                    }
                }
            }
        }
        Vector::new(0.0, 0.0, 0.0)
    }
}
