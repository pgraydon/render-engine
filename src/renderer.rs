use std::f64::consts::PI;
use std::time::Instant;

use image::{Rgb, RgbImage};
use rand::distributions::Uniform;
use rand::prelude::Distribution;
use rand::thread_rng;
use rayon::prelude::*;

use crate::ray::Ray;
use crate::scene::Scene;
use crate::vector::Vector;

const MAX_RAYS: i32 = 64;

pub struct Renderer {
    scene: Scene,
    img_buffer: RgbImage,
    img_height: u32,
    img_width: u32,
}

impl Renderer {
    pub fn new(scene: Scene, img_width: u32, img_height: u32) -> Renderer {
        Renderer {
            scene,
            img_width,
            img_height,
            img_buffer: RgbImage::new(img_width, img_height),
        }
    }

    pub fn render_image(&mut self, path: &str) {
        let uniform = Uniform::new(0.0, 1.0);
        let start = Instant::now();

        self.img_buffer
            .enumerate_pixels_mut()
            .par_bridge()
            .for_each(|(y, x, pixel)| {
                let mut color = Vector::new(0.0, 0.0, 0.0);
                for _ in 0..MAX_RAYS {
                    // Compute perturbatory element for antialiasing
                    let mut rng = thread_rng();
                    let r1: f64 = uniform.sample(&mut rng);
                    let r2: f64 = uniform.sample(&mut rng);

                    let r_log = (-2.0 * r1.ln()).sqrt();
                    let sigma: f64 = 0.5;

                    let gauss_x = r_log * (2.0 * PI * r2).cos() * sigma;
                    let gauss_y = r_log * (2.0 * PI * r2).sin() * sigma;

                    // Compute depth of field
                    let r3: f64 = uniform.sample(&mut rng);
                    let r4: f64 = uniform.sample(&mut rng);

                    let r_log = (-2.0 * r3.ln()).sqrt();
                    let sigma2: f64 = 0.5;

                    let gauss_x2 = r_log * (2.0 * PI * r4).cos() * sigma2;
                    let gauss_y2 = r_log * (2.0 * PI * r4).sin() * sigma2;

                    let mut u = Vector::new(
                        (y as f64 - self.img_width as f64 / 2.0 + gauss_x) as f64,
                        (0.0 - x as f64 + self.img_height as f64 / 2.0 + gauss_y) as f64,
                        (0.0 - self.img_width as f64) / (2.0 * (self.scene.fov / 2.0).tan()),
                    );
                    u.normalize();

                    let p_prime = self.scene.camera + u * self.scene.focus_dist;
                    let o_prime = self.scene.camera + Vector::new(gauss_x2, gauss_y2, 0.0);
                    let mut vec_prime = p_prime - o_prime;
                    vec_prime.normalize();

                    let r = Ray::new(o_prime, vec_prime);
                    color += self.scene.get_color(&r, 10, false);
                }
                color *= 1.0 / MAX_RAYS as f64;

                let red = color.0.powf(0.45).min(255.0) as u8;
                let green = color.1.powf(0.45).min(255.0) as u8;
                let blue = color.2.powf(0.45).min(255.0) as u8;
                *pixel = Rgb([red, green, blue]);
            });

        println!("Time elapsed: {}", start.elapsed().as_millis());

        self.img_buffer.save(path).unwrap();
    }
}
