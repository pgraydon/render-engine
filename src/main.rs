use std::f64::consts::PI;

use render_engine::geometry::{Geometry, Mesh, Sphere, Surface};
use render_engine::renderer::Renderer;
use render_engine::scene::Scene;
use render_engine::vector::Vector;

fn main() {
    let img_width = 512;
    let img_height = 512;

    let camera = Vector::new(0.0, 0.0, 55.0);
    let fov = 60.0 * PI / 180.0;
    let focus_dist = 55.0;

    let light_source = Vector::new(-10.0, 20.0, 40.0);
    let intensity = 1e10;
    let mut scene = Scene::new(camera, fov, focus_dist, intensity, 1.0);

    let sph_light_src = Sphere::new(
        light_source,
        8.0,
        Vector::new(0.0, 0.0, 0.0),
        Surface::LIGHTSOURCE,
        1.0,
        false,
    );
    let sph_left = Sphere::new(
        Vector::new(-15.0, 0.0, 20.0),
        10.0,
        Vector::new(0.7, 0.5, 0.3),
        Surface::MIRROR,
        1.25,
        false,
    );
    let sph_center = Sphere::new(
        Vector::new(0.0, 0.0, 0.0),
        10.0,
        Vector::new(0.5, 0.3, 0.7),
        Surface::TRANSPARENT,
        1.5,
        false,
    );
    let sph_hollow_out = Sphere::new(
        Vector::new(15.0, 0.0, -20.0),
        10.0,
        Vector::new(0.3, 0.7, 0.5),
        Surface::DIFFUSE,
        1.25,
        false,
    );
    let sph_hollow_in = Sphere::new(
        Vector::new(20.0, 0.0, 0.0),
        9.5,
        Vector::new(0.0, 0.0, 0.0),
        Surface::TRANSPARENT,
        1.25,
        true,
    );
    let sph_floor = Sphere::new(
        Vector::new(0.0, -1000.0, 0.0),
        1000.0 - 10.0,
        Vector::new(0.0, 0.0, 1.0),
        Surface::DIFFUSE,
        1.0,
        false,
    );
    let sph_ceiling = Sphere::new(
        Vector::new(0.0, 1000.0, 0.0),
        1000.0 - 60.0,
        Vector::new(1.0, 0.0, 0.0),
        Surface::DIFFUSE,
        1.0,
        false,
    );
    let sph_left_wall = Sphere::new(
        Vector::new(1000.0, 0.0, 0.0),
        1000.0 - 60.0,
        Vector::new(1.0, 1.0, 0.0),
        Surface::DIFFUSE,
        1.0,
        false,
    );
    let sph_right_wall = Sphere::new(
        Vector::new(-1000.0, 0.0, 0.0),
        1000.0 - 60.0,
        Vector::new(0.0, 1.0, 1.0),
        Surface::DIFFUSE,
        1.0,
        false,
    );
    let sph_front_wall = Sphere::new(
        Vector::new(0.0, 0.0, -1000.0),
        1000.0 - 60.0,
        Vector::new(0.0, 1.0, 0.0),
        Surface::DIFFUSE,
        1.0,
        false,
    );
    let sph_back_wall = Sphere::new(
        Vector::new(0.0, 0.0, 1000.0),
        1000.0 - 60.0,
        Vector::new(1.0, 0.0, 1.0),
        Surface::DIFFUSE,
        1.0,
        false,
    );

    let mut cat_mesh = Mesh::from_obj(
        Vector::new(1.0, 1.0, 1.0),
        Surface::DIFFUSE,
        1.0,
        "assets/cat.obj",
    );
    cat_mesh.transform(0.6, &Vector::new(0.0, -10.0, 0.0));
    cat_mesh.compute_bounding_box();

    scene.add_object(Geometry::SPHERE(sph_light_src));

    scene.add_object(Geometry::MESH(cat_mesh));
    //scene.add_object(Geometry::SPHERE(sph_left));
    //scene.add_object(Geometry::SPHERE(sph_center));
    //scene.add_object(Geometry::SPHERE(sph_hollow_out));
    //scene.add_object(Geometry::SPHERE(sph_hollow_in));
    scene.add_object(Geometry::SPHERE(sph_floor));
    scene.add_object(Geometry::SPHERE(sph_ceiling));
    scene.add_object(Geometry::SPHERE(sph_left_wall));
    scene.add_object(Geometry::SPHERE(sph_right_wall));
    scene.add_object(Geometry::SPHERE(sph_front_wall));
    scene.add_object(Geometry::SPHERE(sph_back_wall));

    let mut renderer = Renderer::new(scene, img_width, img_height);
    renderer.render_image("render_bvh_benchmark_64r.png");
}
