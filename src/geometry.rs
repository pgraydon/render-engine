use crate::ray::Ray;
use crate::vector::Vector;
use obj::{load_obj, Obj, Vertex};
use std::fs::File;
use std::io::BufReader;

pub struct Intersection {
    pub intersects: bool,
    pub object_id: usize,
    pub t: f64,
    pub inter_point: Vector,
    pub inter_normal: Vector,
}

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum Surface {
    LIGHTSOURCE,
    MIRROR,
    DIFFUSE,
    TRANSPARENT,
}

pub enum Geometry {
    SPHERE(Sphere),
    MESH(Mesh),
}

impl Geometry {
    pub fn get_surface(&self) -> Surface {
        match self {
            Geometry::SPHERE(s) => s.surface,
            Geometry::MESH(m) => m.surface,
        }
    }

    pub fn get_albedo(&self) -> Vector {
        match self {
            Geometry::SPHERE(s) => s.albedo,
            Geometry::MESH(m) => m.albedo,
        }
    }

    pub fn get_refraction_index(&self) -> f64 {
        match self {
            Geometry::SPHERE(s) => s.n,
            Geometry::MESH(m) => m.n,
        }
    }

    pub fn intersect(&self, r: &Ray) -> Intersection {
        match self {
            Geometry::SPHERE(s) => {
                let direction = r.o - s.c;
                let a: f64 = 1.0;
                let b = 2.0 * r.u.dot(&direction);
                let c = direction.sq_norm() - s.r * s.r;

                let mut intersects = false;
                let mut t: f64 = 0.0;
                let mut inter_point = Vector(0.0, 0.0, 0.0);
                let mut inter_normal = Vector(0.0, 0.0, 0.0);

                let delta = b * b - 4.0 * a * c;
                if delta >= 0.0 {
                    intersects = true;

                    let sqrt_delta = delta.sqrt();

                    let t0 = (-b - sqrt_delta) / 2.0;
                    let t1 = (-b + sqrt_delta) / 2.0;
                    if t1 <= 0.0 {
                        intersects = false;
                    }

                    if t0 > 0.0 {
                        t = t0;
                    } else {
                        t = t1;
                    }

                    inter_point = r.o + r.u * t;
                    inter_normal = inter_point - s.c;
                    inter_normal.normalize();
                    if s.inverted_normal {
                        inter_normal = -inter_normal;
                    }
                }

                Intersection {
                    intersects,
                    t,
                    object_id: 0,
                    inter_point,
                    inter_normal,
                }
            }
            Geometry::MESH(m) => {
                let mut intersects = false;
                let mut t: f64 = f64::MAX;
                let mut inter_point = Vector(0.0, 0.0, 0.0);
                let mut inter_normal = Vector(0.0, 0.0, 0.0);

                if !m.bounding_box.intersect(&r) {
                    return Intersection {
                        intersects,
                        t,
                        object_id: usize::MAX,
                        inter_point,
                        inter_normal,
                    };
                };

                for chunk in m.indices.chunks(3) {
                    let vx_a = vectorize(&m.vertices[chunk[0] as usize]);
                    let vx_b = vectorize(&m.vertices[chunk[1] as usize]);
                    let vx_c = vectorize(&m.vertices[chunk[2] as usize]);
                    let e1 = vx_b - vx_a;
                    let e2 = vx_c - vx_a;
                    let local_inter_normal = e1.cross(&e2);

                    let denom = r.u.dot(&local_inter_normal);
                    let local_t = (vx_a - r.o).dot(&local_inter_normal) / denom;
                    if local_t < 0.0 {
                        continue;
                    }

                    let beta = -r.u.dot(&(vx_a - r.o).cross(&e2)) / denom;
                    if beta < 0.0 || beta > 1.0 {
                        continue;
                    }

                    let gamma = r.u.dot(&(vx_a - r.o).cross(&e1)) / denom;
                    if gamma < 0.0 || gamma > 1.0 {
                        continue;
                    }

                    let alpha = 1.0 - beta - gamma;
                    if alpha < 0.0 {
                        continue;
                    }

                    if local_t < t {
                        t = local_t;
                        intersects = true;
                        inter_normal = local_inter_normal;
                        inter_point = r.o + r.u * t;
                    }
                }
                if intersects {
                    inter_normal.normalize();
                }

                Intersection {
                    intersects,
                    t,
                    object_id: usize::MAX,
                    inter_point,
                    inter_normal,
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct Sphere {
    pub c: Vector,
    pub r: f64,
    pub albedo: Vector,
    pub surface: Surface,
    pub n: f64,
    inverted_normal: bool,
}

impl Sphere {
    pub fn new(
        c: Vector,
        r: f64,
        albedo: Vector,
        surface: Surface,
        n: f64,
        inverted_normal: bool,
    ) -> Sphere {
        Sphere {
            c,
            r,
            albedo,
            surface,
            n,
            inverted_normal,
        }
    }
}

struct BoundingBox {
    b_min: Vector,
    b_max: Vector,
}

impl BoundingBox {
    pub fn intersect(&self, r: &Ray) -> bool {
        let tx1 = (self.b_min.0 - r.o.0) / r.u.0;
        let tx2 = (self.b_max.0 - r.o.0) / r.u.0;
        let tx_min = tx1.min(tx2);
        let tx_max = tx1.max(tx2);

        let ty1 = (self.b_min.1 - r.o.1) / r.u.1;
        let ty2 = (self.b_max.1 - r.o.1) / r.u.1;
        let ty_min = ty1.min(ty2);
        let ty_max = ty1.max(ty2);

        let tz1 = (self.b_min.2 - r.o.2) / r.u.2;
        let tz2 = (self.b_max.2 - r.o.2) / r.u.2;
        let tz_min = tz1.min(tz2);
        let tz_max = tz1.max(tz2);

        tx_max.min(ty_max.min(tz_max)) > tx_min.max(ty_min.max(tz_min))
    }
}

fn vectorize(vx: &Vertex) -> Vector {
    Vector::new(vx.position[0] as f64, vx.position[1] as f64, vx.position[2] as f64)
}

pub struct Mesh {
    pub albedo: Vector,
    pub surface: Surface,
    pub n: f64,
    pub vertices: Vec<Vertex>,
    pub indices: Vec<u16>,
    bounding_box: BoundingBox,
}

impl Mesh {
    pub fn from_obj(albedo: Vector, surface: Surface, n: f64, path: &str) -> Self {
        let input = BufReader::new(File::open(path).unwrap());
        let obj: Obj = load_obj(input).unwrap();

        Mesh {
            albedo,
            surface,
            n,
            vertices: obj.vertices,
            indices: obj.indices,
            bounding_box: BoundingBox {
                b_min: Vector::new(0.0, 0.0, 0.0),
                b_max: Vector::new(0.0, 0.0, 0.0),
            },
        }
    }

    pub fn transform(&mut self, scale: f64, translation: &Vector) {
        self.vertices.iter_mut().for_each(|mut vx| {
            vx.position[0] = vx.position[0] * scale as f32 + translation.0 as f32;
            vx.position[1] = vx.position[1] * scale as f32 + translation.1 as f32;
            vx.position[2] = vx.position[2] * scale as f32 + translation.2 as f32;
        })
    }

    pub fn compute_bounding_box(&mut self) {
        self.bounding_box.b_max = Vector::new(-1.0e9, -1.0e9, -1.0e9);
        self.bounding_box.b_min = Vector::new(1.0e9, 1.0e9, 1.0e9);
        self.vertices.iter().for_each(|vx| {
            self.bounding_box.b_max.0 = (vx.position[0] as f64).max(self.bounding_box.b_max.0);
            self.bounding_box.b_min.0 = (vx.position[0] as f64).min(self.bounding_box.b_min.0);

            self.bounding_box.b_max.1 = (vx.position[1] as f64).max(self.bounding_box.b_max.1);
            self.bounding_box.b_min.1 = (vx.position[1] as f64).min(self.bounding_box.b_min.1);

            self.bounding_box.b_max.2 = (vx.position[2] as f64).max(self.bounding_box.b_max.2);
            self.bounding_box.b_min.2 = (vx.position[2] as f64).min(self.bounding_box.b_min.2);
        })
    }
}
