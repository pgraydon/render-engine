use crate::vector::Vector;

pub struct Ray {
    pub o: Vector,
    pub u: Vector,
}

impl Ray {
    pub fn new(o: Vector, u: Vector) -> Self {
        Ray { o, u }
    }
}
